import { createStore } from "redux";

// 1st parameter is the structure of the store
let initStoreValue = 100;
const reducer = (state = { sharedCounter: initStoreValue }, action) => {

    if (action.type === "add") {
        const newValue = state.sharedCounter + action.amount;
        const newState = { sharedCounter: newValue };
        return newState;
    }

    if (action.type === "subtract") {
        const newValue = state.sharedCounter - 1;
        const newState = { sharedCounter: newValue };
        return newState;
    }

    return state;

}

const store = createStore(reducer);

export default store;