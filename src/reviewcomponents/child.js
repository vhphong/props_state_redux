import { useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

export default function Child(props) {

    const name = props.fName; // props

    let initValue = 12345;
    const [counter, setCounter] = useState(initValue); // state

    const sharedCounter = useSelector(state => state.sharedCounter); // redux
    const dispatch = useDispatch();

    function addOneToCounterState() {
        const newValue = counter + 1;
        setCounter(newValue);
    }

    function addToSharedCounter() {
        dispatch({ type: "add", amount: 50 }); // send an action to the reducer function
    }

    function subtractToSharedCounter() {
        dispatch({ type: "subtract" }); // send an action to the reducer function
    }




    return (
        <div>
            <h3>My name is {name}</h3>
            <h3>Stateful counter {counter}</h3>
            <h3>Shared state counter {sharedCounter}</h3>
            <button onClick={addOneToCounterState}>Add to state</button>
            <button onClick={addToSharedCounter}>Add to shared counter</button>
            <button onClick={subtractToSharedCounter}>Subtract to shared counter</button>
        </div>)

}